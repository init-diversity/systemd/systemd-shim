# systemd-shim for debian based multi-init distros.

All credits for maintaining this fork need to go to the MX Linux development team.

This repo contains a fork of systemd-shim.  
It builds a Debian Buster/Bullseye/Bookworm based debian package for MX & other multi-init debian distro's.

It depends on a modified systemd available here:
- https://gitlab.com/init-diversity/systemd/systemd-init-diversity.git
- https://github.com/knelsonmeister/systemd)

## Background:
Systemd-shim allows a system to boot up using a SYSV or other inits, but still be able to support the modern programs that depend on systemd.  
It is not perfect, but it allows for a Linux distribution to support init-diversity with systemd as a choice in the grub boot menu.  
Debian used to support systemd-shim, but support was dropped in Debian Buster. 

## Current Version: 10-6
Based on:
- https://mxrepo.com/mx/repo/pool/main/s/systemd-shim/systemd-shim_10-6.debian.tar.xz
- https://github.com/knelsonmeister/systemd-shim
- https://github.com/desrt/systemd-shim

## Changes:
  - Added new patch: buster-support
      - Patch adds supports for querying LoadState needed to reboot/shutdown
  - Updated debian/patches/series file to add new patch
  - Updated debian/changelog to increment version to 10-6
  - Removed dependency on cgmanager which was removed from bookworm.

## How To Build:
The recommended method to build this package directly from git:

```
sudo apt install git-buildpackage #alongside-Build-Depends &&
gbp clone https://gitlab.com/init-diversity/systemd/systemd-shim.git && 
cd systemd-shim && 
gbp buildpackage -uc -us

```

If compiling using antiX or Devuan or simply do not prefer to compile while booted in systemd you will need to set-up a chroot (recommended method):
```
sudo apt install git-buildpackage pbuilder cowbuilder qemu-user-static &&
sudo DIST=stable ARCH=amd64 git-pbuilder create &&
sudo DIST=stable ARCH=amd64 git-pbuilder login --save-after-login
```
While in chroot:
```
apt update && apt upgrade && exit
```
Once the this has been created, a non-systemd user can build using:
```
gbp clone https://gitlab.com/init-diversity/systemd/systemd-init-diversity.git && 
cd systemd-init-diversity && 
gbp buildpackage --git-pbuilder --git-dist=stable --git-arch=amd64 -uc -us
```

